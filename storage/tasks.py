from datetime import datetime, timezone

# Expiration period in seconds
TIME_PERIOD = 24 * 60 * 60


def get_delta(page):
    now = datetime.now(timezone.utc)
    image_date = page.time
    return now - image_date


