FROM python:3.9-alpine AS compile-image

RUN apk update && \
    apk add postgresql-dev \
    g++ \
    gcc  \
    python3-dev  \
    musl-dev  \
    libressl-dev musl-dev \
    libffi-dev \
    zlib-dev \
    freetype-dev \
    jpeg-dev

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

COPY pyproject.toml .
COPY poetry.lock .

RUN pip install poetry
RUN poetry config virtualenvs.create false --local
RUN poetry check && poetry install -vvv
RUN pip check

FROM python:3.9-alpine AS build-image

COPY --from=compile-image /opt/venv /opt/venv

RUN apk add --no-cache \
    libpq \
    libjpeg \
    freetype-dev

ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /app

COPY . .

ENTRYPOINT ["/app/entrypoint.sh"]